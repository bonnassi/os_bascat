# WELCOME TO THE BASCAT ROBOT

This semester, our team built BASCAT the robot, the best non-human basketball robot !! BASCAT is a robot based on the LEGO MINDSTORMS EV3, whose purpose is to win basketball matches by 
score baskets. 

### Prerequisites 

First of all, you'll need to install Linux on your ev3 robot: https://www.ev3dev.org/docs/getting-started/ .
Then, you have two solutions : either you download our code on your computer, cross-compile it and send the executables to the robot. either you download our code, compile it and launch it on the robot.

HOW CAN I CROSS-COMPILE ?

Follow the instructions about cross-compilation on the website : http://soc.eurecom.fr/OS/projects_fall2018.html

Now that you know how to cross-compile, let's try our code

First of all, clone the BASCAT directory : GIT_SSL_NO_VERIFY=true git clone https://github.com/bonnassi/os_bascat.git

### The architecture 

We used 4 sensors (a color sensor, an ultrasonic sensor and a gyroscope sensor, yes there are 3 sensors actually) and 4 motors (to move, to catch the ball and to launch the ball).


### The Team

The leader : BONNASSIEUX Julie

Her devoted team : AHMED-KHALIFA Abbe / MEALLIER Jordan / TRONCY Pauline


### Thanks

We'd like to thank our teacher Ludovic Aprville and Matteo Bertolino for helping us by advising us and the last generations for helping us to know how to begin in this project.



*return to our website : http://bascat.strikingly.com/*
