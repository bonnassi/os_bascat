#include <stdio.h>
#include <stdlib.h>
#include "ev3.h"
#include "ev3_port.h"
#include "ev3_tacho.h"
#include "ev3_sensor.h"

#define Sleep( msec ) usleep(( msec ) * 1000 )

void only_catch_keep()
{
  uint8_t sn;
  FLAGS_T state;
  int port=65;
  if ( ev3_search_tacho_plugged_in(port,0, &sn, 0 )) {
      int max_speed;

      printf( "LEGO_EV3_M_M00OTOR PINCE is found, run for 5 sec...\n" );
      get_tacho_max_speed( sn, &max_speed );
      printf("  max speed = %d\n", max_speed );
      set_tacho_stop_action_inx( sn, TACHO_COAST );

      set_tacho_speed_sp( sn, max_speed*0.1 );
      set_tacho_time_sp( sn, 900 );
      //set_tacho_ramp_up_sp( sn, 400 );
      //set_tacho_ramp_down_sp( sn, 300 );
      set_tacho_command_inx( sn, TACHO_RUN_TIMED );
      do {
        get_tacho_state_flags( sn, &state );
      } while ( state );

      printf( "on monte la balle\n" );
      get_tacho_state_flags( sn, &state );
      printf("Here is the robot position : %d\n", state);
      set_tacho_speed_sp( sn, max_speed*0.3 );
      set_tacho_time_sp( sn, 600 );
      //set_tacho_ramp_up_sp( sn, 50 );
      //set_tacho_ramp_down_sp( sn, 50 );
      //set_tacho_position_sp( sn, 90 );
      set_tacho_command_inx( sn, TACHO_RUN_TIMED );
        Sleep(2000);
        printf("We got down 2");
      do {
        get_tacho_state_flags( sn, &state );
      } while ( state );

    } else {
      printf( "LEGO_EV3_M_MOTOR 1 is NOT found\n" );
    }

  Sleep( 1000 );

}

