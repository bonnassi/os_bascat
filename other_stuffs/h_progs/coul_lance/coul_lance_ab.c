#include <stdio.h>
#include <stdlib.h>
#include "ev3.h"
#include "ev3_port.h"
#include "ev3_tacho.h"
#include "ev3_sensor.h"

#include <unistd.h>

const char const *color[] = { "?", "BLACK", "BLUE", "GREEN", "YELLOW", "RED", "WHITE", "BROWN" };
#define COLOR_COUNT  (( int )( sizeof( color ) / sizeof( color[ 0 ])))

#define Sleep( msec ) usleep(( msec ) * 1000 )

void coul_lance_ab( )
{
  uint8_t sn_color;
  int val;

  while ( ev3_tacho_init() < 1 ) Sleep( 1000 );

  //Run color sensor
  ev3_sensor_init();

  for (int i = 0 ; i < 10 ; i++){
    if ( ev3_search_sensor( LEGO_EV3_COLOR, &sn_color, 0 )) {
      printf( "COLOR sensor is found, reading COLOR...\n" );
      if ( !get_sensor_value( 0, sn_color, &val ) || ( val < 0 ) || ( val >= COLOR_COUNT )) {
        val = 0;
      }
      printf( "\r(%s) \n", color[ val ]);
      if (val == 0) {
        printf("\r Well there is no ball \n");
        break;
      }
      fflush( stdout );
      Sleep(200);
    }
  }

}

