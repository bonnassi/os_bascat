#include <stdio.h>
#include <stdlib.h>
#include "ev3.h"
#include "ev3_port.h"
#include "ev3_tacho.h"
#include "ev3_sensor.h"
#include "ev3_dc.h"
#include <pthread.h>
#include <math.h>
#include <time.h>
#include "find_catch_ab.h"
#include "catch_throw_ab.h"
#include "give_ball_ab.h"
#include "coul_lance_ab.h"
#include "test_1_lance_ab.h"
#include "only_catch_keep_ab.h"
#include "give_ball_ab.h"

 
// WIN32 /////////////////////////////////////////
#ifdef __WIN32__

#include <windows.h>

// UNIX //////////////////////////////////////////
#else

#include <unistd.h>
#define Sleep( msec ) usleep(( msec ) * 1000 )

//////////////////////////////////////////////////
#endif


int flag_killer=0;
int motor_init(uint8_t *motor0, uint8_t *motor1, uint8_t *motor2, uint8_t *motor3); 
uint8_t motors[4];
//déclaration des threads et de leurs fonctions associées
void *movements();
pthread_t thread_movement;
void *detection();
pthread_t thread_detection;
pthread_mutex_t mov_lock;
pthread_mutex_t detect_lock;
float value;



#define COMP_SX -1.0//0.9975 or 1.017
#define COMP_DX -1.0000

#define MOT_DIR 1
#define MOT_PINCE 65
#define MOT_CATAPULTE 66
#define MOT_SX 67
#define MOT_DX 68
#define MAX_SPEED 1050
#define STOP_ACTION TACHO_BRAKE



#define TURN360 500

#define WHEEL_RADIUS 0.0275



typedef struct position {
	volatile float x;
	volatile float y;
	volatile float dir;
} position;

position my_pos;




//Code correspondant aux threads
//thread du mouvement
void* movements()
{
  	printf( "We are entering in the movements thread!\n" );
  	printf( "***\n" );
	printf("*M* The first position was : \n");
	printf("*M* x: %f , y: %f, dir: %f \n", my_pos.x, my_pos.y, my_pos.dir);

	time_t t0 = clock();
	int aller = 0;
	pthread_mutex_lock(&mov_lock);
	for ( ; ; ){
		go_forwards_cm_forever(motors, MAX_SPEED/10);
		//printf("*M* The current position is : \n");
		//printf("*M* x: %f , y: %f, dir: %f \n", my_pos.x, my_pos.y, my_pos.dir);
		//printf("difference time : %ld", clock() - t0);
		//my_pos.x = my_pos.x + cm * sin((my_pos.dir * M_PI) / 180.0);
		//my_pos.y = my_pos.y + cm * cos((my_pos.dir * M_PI) / 180.0);
		if (pthread_mutex_trylock(&detect_lock) == 0) {
			set_tacho_command_inx( motors[0], TACHO_STOP );
			set_tacho_command_inx( motors[1], TACHO_STOP );
			printf( "*M* Detection m'a dit de m'arreter!\n" );
			printf("*Main* The current position is : \n");
			printf("*Main* x: %f , y: %f, dir: %f \n", my_pos.x, my_pos.y, my_pos.dir);
			printf("*Main* elle avait été détectée à : %f mm\n", value);
	
			if (value > 20) {
				va_chercher_maintenant(motors);
			} else {
				va_chercher_ac_creneau(motors);
			}
			grabtheball(motors);
			break;

		} else if (clock() - t0 > 3684540 && aller == 0) {
			set_tacho_command_inx( motors[0], TACHO_STOP );
			set_tacho_command_inx( motors[1], TACHO_STOP );
			printf("*Main* x: %f , y: %f, dir: %f \n", my_pos.x, my_pos.y, my_pos.dir);
			printf( "*M* didn't find anything on this side!\n" );
			turn_right_motors(motors, MAX_SPEED/6, 185);
			printf("*Main* x: %f , y: %f, dir: %f \n", my_pos.x, my_pos.y, my_pos.dir);
			t0 = clock();
			aller +=1;
		} else if (clock() - t0 > 3684540 && aller == 1) {
			set_tacho_command_inx( motors[0], TACHO_STOP );
			set_tacho_command_inx( motors[1], TACHO_STOP );
			printf("*Main* x: %f , y: %f, dir: %f \n", my_pos.x, my_pos.y, my_pos.dir);
			printf( "*M* didn't find anything on this side either!\n" );
			pthread_mutex_unlock(&mov_lock);
			break;
		}/* else if (i==99) {
			set_tacho_command_inx( motors[0], TACHO_STOP );
			set_tacho_command_inx( motors[1], TACHO_STOP );	
			printf("*Main* x: %f , y: %f, dir: %f \n", my_pos.x, my_pos.y, my_pos.dir);
			pthread_mutex_unlock(&mov_lock);		
			break;
		}*/
	}
        return EXIT_SUCCESS;
}
//thread de la détection
void* detection()
{
  	printf( "We are entering in the detection thread!\n" );
	uint8_t sn_sonar;
	pthread_mutex_lock(&detect_lock);
	for ( ; ; ){
		if (ev3_search_sensor(LEGO_EV3_US, &sn_sonar,0)){
		      //printf("*D* SONAR found, reading sonar...\n");
		      if ( !get_sensor_value0(sn_sonar, &value )) {
			value = 0;
		      }
		      value = value/10;
		      //printf( "*D* sonar :\r(%f) \n", value);
		      fflush( stdout );
		}
		//printf("%f",value);
		//difference = 600 - value;
		//printf("%f",difference);
		if (value < 0.45) {  //if (value < 45) {
		      	printf( "*D* c'est tout pour moi! \n");
			pthread_mutex_unlock(&detect_lock);
			break;
		}
		else if (pthread_mutex_trylock(&mov_lock) == 0) {
			printf( "*D* Movement m'a dit de m'arreter!\n" );
			break;
		}
	}
        return EXIT_SUCCESS;
}



//le fameux main
int main( void )
{
	  int i;
	  uint8_t sn_touch;
	  char s[ 256 ];
	  int val;
	  uint32_t n, ii;
	  //float value;


	  //struct timeb t0, t1;
	  //struct thread_arguments thread_args;

	#ifndef __ARM_ARCH_4T__
	  /* Disable auto-detection of the brick (you have to set the correct address below) */
	  ev3_brick_addr = "192.168.0.204";

	#endif
	  if ( ev3_init() == -1 ) return ( 1 );

	#ifndef __ARM_ARCH_4T__
	  printf( "The EV3 brick auto-detection is DISABLED,\nwaiting %s online with plugged tacho...\n", ev3_brick_addr );

	#else
	  printf( "Waiting tacho is plugged...\n" );

	#endif
	  while ( ev3_tacho_init() < 1 ) Sleep( 1000 );

	  printf( "*** ( EV3 ) Hello! ***\n" );

	  printf( "Found tacho motors:\n" );
	  for ( i = 0; i < DESC_LIMIT; i++ ) {
	    if ( ev3_tacho[ i ].type_inx != TACHO_TYPE__NONE_ ) {
	      printf( "  type = %s\n", ev3_tacho_type( ev3_tacho[ i ].type_inx ));
	      printf( "  port = %s\n", ev3_tacho_port_name( i, s ));
	      printf("  port = %d %d\n", ev3_tacho_desc_port(i), ev3_tacho_desc_extport(i));
	    }
	  }

	  //Run all sensors
	  ev3_sensor_init();

	  printf( "Found sensors:\n" );
	  for ( i = 0; i < DESC_LIMIT; i++ ) {
	    if ( ev3_sensor[ i ].type_inx != SENSOR_TYPE__NONE_ ) {
	      printf( "  type = %s\n", ev3_sensor_type( ev3_sensor[ i ].type_inx ));
	      printf( "  port = %s\n", ev3_sensor_port_name( i, s ));
	      if ( get_sensor_mode( i, s, sizeof( s ))) {
		printf( "  mode = %s\n", s );
	      }
	      if ( get_sensor_num_values( i, &n )) {
		for ( ii = 0; ii < n; ii++ ) {
		  if ( get_sensor_value( ii, i, &val )) {
		    printf( "  value%d = %d\n", ii, val );
		  }
		}
	      }
	    }
	  }
	  if ( ev3_search_sensor( LEGO_EV3_TOUCH, &sn_touch, 0 )) {
	    printf( "TOUCH sensor is found, press BUTTON for EXIT...\n" );
	  }

	  motor_init( &motors[0], &motors[1], &motors[2], &motors[3]);

	  my_pos.x = 0;
	  my_pos.y = 0;
	  my_pos.dir = 0;


	  

	  ev3_uninit();
	  give();
	  printf( "*** ( EV3 ) Bye! ***\n" );

	  return ( 0 );
}



//methode définissant reliant les moteurs physique à leur représentation dans le code
int motor_init(uint8_t *motor0, uint8_t* motor1, uint8_t* motor2, uint8_t* motor3) 
{
	int all_ok = 1;
	ev3_tacho_init();
	//initialisation moteur gauche
	if ( !ev3_search_tacho_plugged_in( MOT_SX, 0, motor0, 0 )) {
		fprintf( stderr, "Motor SX not found!\n" );
		set_tacho_stop_action_inx( *motor0, STOP_ACTION );
		set_tacho_command_inx( *motor0, TACHO_STOP );
		all_ok = 0;
	} else {
		set_tacho_stop_action_inx( *motor0, STOP_ACTION );
		set_tacho_command_inx( *motor0, TACHO_STOP );
	}
	//initialisation moteur droite
	if ( !ev3_search_tacho_plugged_in( MOT_DX, 0, motor1, 0 )) {
		fprintf( stderr, "Motor DX not found!\n" );
		set_tacho_stop_action_inx( *motor1, STOP_ACTION );
		set_tacho_command_inx( *motor1, TACHO_STOP );
		all_ok = 0;
	} else {
		set_tacho_stop_action_inx( *motor1, STOP_ACTION );
		set_tacho_command_inx( *motor1, TACHO_STOP );
	}
	//intialisation moteur pince
	if ( !ev3_search_tacho_plugged_in( MOT_PINCE, 0, motor2, 0 )) {
		fprintf( stderr, "Motor pince not found!\n" );
		set_tacho_stop_action_inx( *motor2, STOP_ACTION );
		set_tacho_command_inx( *motor2, TACHO_STOP );
		all_ok = 0;
	} else {
		set_tacho_stop_action_inx( *motor2, STOP_ACTION );
		set_tacho_command_inx( *motor2, TACHO_STOP );
	}
	//initialisation moteur catapulte
	if ( !ev3_search_tacho_plugged_in( MOT_CATAPULTE, 0, motor3, 0 )) {
		fprintf( stderr, "Motor catapulte not found!\n" );
		set_tacho_stop_action_inx( *motor3, STOP_ACTION );
		set_tacho_command_inx( *motor3, TACHO_STOP );
		all_ok = 0;
	} else {
		set_tacho_stop_action_inx( *motor3, STOP_ACTION );
		set_tacho_command_inx( *motor3, TACHO_STOP );
	}
	if (all_ok){
		stop_motor(*motor0);
		stop_motor(*motor1);
		stop_motor(*motor2);
		stop_motor(*motor3);
	}
	return all_ok;
}



