//Pauline

#include <stdio.h>
#include <stdlib.h>
#include "ev3.h"
#include "ev3_port.h"
#include "ev3_tacho.h"
#include "ev3_sensor.h"
// WIN32 /////////////////////////////////////////
#ifdef __WIN32__

#include <windows.h>

// UNIX //////////////////////////////////////////
#else

#include <unistd.h>
#define Sleep( msec ) usleep(( msec ) * 1000 )

//////////////////////////////////////////////////
#endif

const char const *color[] = { "?", "BLACK", "BLUE", "GREEN", "YELLOW", "RED", "WHITE", "BROWN" };
#define COLOR_COUNT  (( int )( sizeof( color ) / sizeof( color[ 0 ])))

int main( void )
{
  int doilaunch;
  int i;
  uint8_t sn;
  FLAGS_T state;
  char s[ 256 ];
  uint8_t sn_color;
  int val;

#ifndef __ARM_ARCH_4T__
  /* Disable auto-detection of the brick (you have to set the correct address below) */
  ev3_brick_addr = "10.0.42.82";

#endif
  if ( ev3_init() == -1 ) return ( 1 );

#ifndef __ARM_ARCH_4T__
  printf( "The EV3 brick auto-detection is DISABLED,\nwaiting %s online with plugged tacho...\n", ev3_brick_addr );

#else
  printf( "Waiting tacho is plugged...\n" );

#endif
  while ( ev3_tacho_init() < 1 ) Sleep( 1000 );

  printf( "*** ( EV3 ) Hello! ***\n" );

  printf( "Found tacho motor:\n" );
  for ( i = 0; i < DESC_LIMIT; i++ ) {
    if ( ev3_tacho[ i ].type_inx != TACHO_TYPE__NONE_ ) {
      printf( "  type = %s\n", ev3_tacho_type( ev3_tacho[ i ].type_inx ));
      printf( "  port = %s\n", ev3_tacho_port_name( i, s ));
      printf("  port = %d %d\n", ev3_tacho_desc_port(i), ev3_tacho_desc_extport(i));
    }
  }

  //Run color sensor
  ev3_sensor_init();

  for (i = 0 ; i < 3 ; i++){
    if ( ev3_search_sensor( LEGO_EV3_COLOR, &sn_color, 0 )) {
      printf( "COLOR sensor is found, reading COLOR...\n" );
      if ( !get_sensor_value( 0, sn_color, &val ) || ( val < 0 ) || ( val >= COLOR_COUNT )) {
        val = 0;
      }
      printf( "\r(%s) \n", color[ val ]);
      if (val == 0) {
        printf("\r Well there is no ball \n");
        doilaunch = 1;
        break;
      }
      fflush( stdout );
      Sleep(200);
    }
    doilaunch = 0;
  }

  int port=66;
  //PARTIE CATAPULTE
  if ( ev3_search_tacho_plugged_in(port,0, &sn, 0 ) && doilaunch == 0) {
      int max_speed;

      printf( "LEGO_EV3_M_MOTOR 1 is found, run for 5 sec...\n" );
      get_tacho_max_speed( sn, &max_speed );
      printf("  max speed = %d\n", max_speed );
      set_tacho_stop_action_inx( sn, TACHO_COAST );
      set_tacho_speed_sp( sn, max_speed*0.2);
      set_tacho_time_sp( sn, 400 );
      set_tacho_ramp_down_sp( sn, 500 );
      set_tacho_command_inx( sn, TACHO_RUN_TIMED );
      do {
        get_tacho_state_flags( sn, &state );
      } while ( state );
      Sleep( 100 );
      printf( "on essaye dans l'autre sens\n" );
      set_tacho_speed_sp( sn, - max_speed*0.1 );
      set_tacho_time_sp( sn, 900 );
      set_tacho_ramp_up_sp( sn, 10 );
      set_tacho_ramp_down_sp( sn, 300 );
      set_tacho_command_inx( sn, TACHO_RUN_TIMED );
      do {
        get_tacho_state_flags( sn, &state );
      } while ( state );

    } else {
      if (doilaunch == 1) {
        printf("\rThere were no ball, so I did not launch anything\n");
      } else {
        printf("\rLEGO_EV3_M_MOTOR FOR LAUNCHING is NOT found\n" );
      }
    }

  
  ev3_uninit();
  printf( "*** ( EV3 ) Bye! ***\n" );

  return ( 0 );
}

