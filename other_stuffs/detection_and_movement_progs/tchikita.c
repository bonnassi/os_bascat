//Abbe

#include <stdio.h>
#include <stdlib.h>
#include "ev3.h"
#include "ev3_port.h"
#include "ev3_tacho.h"
#include "ev3_sensor.h"
#include "ev3_dc.h"
#include "brick.h"
#include <pthread.h>
#include <math.h>


// WIN32 /////////////////////////////////////////
#ifdef __WIN32__

#include <windows.h>

// UNIX //////////////////////////////////////////
#else

#include <unistd.h>
#define Sleep( msec ) usleep(( msec ) * 1000 )

//////////////////////////////////////////////////
#endif
const char const *color[] = { "?", "BLACK", "BLUE", "GREEN", "YELLOW", "RED", "WHITE", "BROWN" };
#define COLOR_COUNT  (( int )( sizeof( color ) / sizeof( color[ 0 ])))


int flag_killer=0;
int motor_init(uint8_t *motor0, uint8_t *motor1, uint8_t *motor2, uint8_t *motor3); 
uint8_t motors[4];
//déclaration des threads et de leurs fonctions associées
void *movements(void *arg);
pthread_t thread_movement;
void *detection(void * args);
pthread_t thread_detection;
pthread_mutex_t detect_lock;
float d;


#define MY_ID 1
#define START_X 60
#define START_Y 20
#define START_DIR 0

#define COMP_SX -1.0//0.9975 or 1.017
#define COMP_DX -1.0000

#define MOT_DIR 1
#define MOT_PINCE 65
#define MOT_CATAPULTE 66
#define MOT_SX 67
#define MOT_DX 68
#define MAX_SPEED 1050
#define STOP_ACTION TACHO_BRAKE

#define WHEEL_DIAM 55

#define MOV_RAMP_UP 0
#define MOV_RAMP_DOWN 0

#define TURN360 600

#define WHEEL_RADIUS 0.0275

#define ROBOT_WIDTH 14

#define L 250
#define H 330

#define P 10

#define ROT_THRESHOLD 85

typedef struct position {
	volatile float x;
	volatile float y;
	volatile float dir;
} position;

position my_pos;



//methodes utilisée pour faire bouger le robot
void wait_motor_stop(uint8_t motor) {
 	FLAGS_T state;
 	do {
 	    get_tacho_state_flags( motor, &state );
 	} while ( state );
}

void turn_right(uint8_t *motors, int speed, int deg) {
	multi_set_tacho_stop_action_inx( motors, STOP_ACTION );
	multi_set_tacho_speed_sp( motors, speed );
	multi_set_tacho_ramp_up_sp( motors, 0 );
	multi_set_tacho_ramp_down_sp( motors, 0 );

	//set_tacho_speed_sp( motors[0], speed * COMP_SX);
	//set_tacho_speed_sp( motors[1], speed * COMP_DX);

	set_tacho_position_sp( motors[0], MOT_DIR*(TURN360*deg)/360 );
	set_tacho_position_sp( motors[1], -MOT_DIR*(TURN360*deg)/360 );

        //set_tacho_command_inx( motors[0], TACHO_RUN_TIMED );
	//set_tacho_command_inx( motors[1], TACHO_RUN_TIMED );

	multi_set_tacho_command_inx( motors, TACHO_RUN_TO_REL_POS );
}

void turn_right_motors(uint8_t *motors, int speed, int deg) {
	turn_right(motors, speed, deg);
	wait_motor_stop(motors[0]); wait_motor_stop(motors[1]);
	my_pos.dir = ((((int)my_pos.dir + deg) % 360 ) + 360 ) % 360;
	if (my_pos.dir > 180) {
		my_pos.dir -= 360;
	}
}

void stop_motor(uint8_t motor) {
	set_tacho_command_inx( motor, TACHO_STOP );
}

void go_forwards_cm_forever(uint8_t *motors, int cm, int speed) {
	// float time = ((360.0*cm)/(2*M_PI*WHEEL_RADIUS)/speed)*10;

	multi_set_tacho_stop_action_inx( motors, STOP_ACTION );
	set_tacho_speed_sp( motors[0], speed * COMP_SX);
	set_tacho_speed_sp( motors[1], speed * COMP_DX);
	// set_tacho_time_sp( motors[0], time );
	// set_tacho_time_sp( motors[1], time );
         set_tacho_command_inx( motors[0], TACHO_RUN_FOREVER);
	 set_tacho_command_inx( motors[1], TACHO_RUN_FOREVER );
	
	//wait_motor_stop(motors[0]);
	//wait_motor_stop(motors[1]);

	my_pos.x = my_pos.x + cm * sin((my_pos.dir * M_PI) / 180.0);
	my_pos.y = my_pos.y + cm * cos((my_pos.dir * M_PI) / 180.0);

}

void go_forwards_cm_foratime(uint8_t *motors, float cm, int speed) {
	float time = ((360.0*cm)/(2*M_PI*WHEEL_RADIUS)/speed)*10;

	multi_set_tacho_stop_action_inx( motors, STOP_ACTION );
	set_tacho_speed_sp( motors[0], speed * COMP_SX);
	set_tacho_speed_sp( motors[1], speed * COMP_DX);
	set_tacho_time_sp( motors[0], time );
	set_tacho_time_sp( motors[1], time );

	set_tacho_command_inx( motors[0], TACHO_RUN_TIMED);
	set_tacho_command_inx( motors[1], TACHO_RUN_TIMED );
   
	
	wait_motor_stop(motors[0]);
	wait_motor_stop(motors[1]);

	my_pos.x = my_pos.x + cm * sin((my_pos.dir * M_PI) / 180.0);
	my_pos.y = my_pos.y + cm * cos((my_pos.dir * M_PI) / 180.0);

}

void grabtheball (uint8_t *motors){
  FLAGS_T state;	
  //PARTIE PINCE
      int max_speed;
      printf( "PINCE is found, run for 5 sec...\n" );
      get_tacho_max_speed( motors[2], &max_speed );
      printf("  max speed = %d\n", max_speed );
      set_tacho_stop_action_inx( motors[2], TACHO_COAST );

      set_tacho_speed_sp( motors[2], max_speed*0.1 );
      set_tacho_time_sp( motors[2], 900 );
      set_tacho_command_inx( motors[2], TACHO_RUN_TIMED );
      do {
        get_tacho_state_flags( motors[2], &state );
      } while ( state );

      printf( "on monte la balle\n" );
      get_tacho_state_flags( motors[2], &state );
      printf("Here is the robot position : %d\n", state);
      set_tacho_speed_sp( motors[2], max_speed*0.3 );
      set_tacho_time_sp( motors[2], 600 );
      set_tacho_command_inx( motors[2], TACHO_RUN_TIMED );
	Sleep(2000);
	printf("We got down 2");
      do {
        get_tacho_state_flags( motors[2], &state );
      } while ( state );

      /* Wait tacho stop */
      Sleep( 1000 );
      printf( "on essaye dans l'autre sens\n" );
      get_tacho_state_flags( motors[2], &state );
      printf("Here is the robot position : %d\n", state);
      set_tacho_speed_sp( motors[2], - max_speed*0.3 );
      set_tacho_time_sp( motors[2], 800 );
      set_tacho_command_inx( motors[2], TACHO_RUN_TIMED );
	Sleep(2000);
	printf("We got down 2");
      do {
        get_tacho_state_flags( motors[2], &state );
      } while ( state );
      Sleep( 1000 );
	printf("We got down");
}

void va_chercher_maintenant(uint8_t *motors) {
	go_forwards_cm_foratime(motors, 17, MAX_SPEED/10);
	printf("*Main* J'avance un peu par ce que sinon c'est la roue qui est centrée - The current position is : \n");
	printf("*Main* x: %f , y: %f, dir: %f \n", my_pos.x, my_pos.y, my_pos.dir);
	turn_right_motors(motors, MAX_SPEED/6, 90);
	printf("*Main* The current position is : \n");
	printf("*Main* x: %f , y: %f, dir: %f \n", my_pos.x, my_pos.y, my_pos.dir);
	go_forwards_cm_foratime(motors, (value - 12), MAX_SPEED/30);
	printf("*Main* The current position is : \n");
	printf("*Main* x: %f , y: %f, dir: %f \n", my_pos.x, my_pos.y, my_pos.dir);
	printf( "*M* VAZI CA SE TENTE!\n" );
}

void va_chercher_ac_creneau(uint8_t *motors) {
	go_backwards_cm_foratime(motors, 30-17, MAX_SPEED/10);
	printf("*Main* x: %f , y: %f, dir: %f \n", my_pos.x, my_pos.y, my_pos.dir);

	float deg = atanf(value/30)* 180.0f / M_PI;
	turn_right_motors(motors, MAX_SPEED/6, deg);
	printf("*Main* x: %f , y: %f, dir: %f \n", my_pos.x, my_pos.y, my_pos.dir);

	float d = hypotf(30, value);
	go_forwards_cm_foratime(motors, d - 12, MAX_SPEED/30);
	printf("*Main* x: %f , y: %f, dir: %f \n", my_pos.x, my_pos.y, my_pos.dir);
	printf( "*M* VAZI CA SE TENTE!\n" );
}

//Code correspondant aux threads
//thread du mouvement
void* movements()
{
  	printf( "We are entering in the movements thread!\n" );
  	printf( "***\n" );
	printf("*M* The first position was : \n");
	printf("*M* x: %f , y: %f, dir: %f \n", my_pos.x, my_pos.y, my_pos.dir);

	time_t t0 = clock();
	int aller = 0;

	for ( ; ; ){
		go_forwards_cm_forever(motors, MAX_SPEED/10);
		//printf("*M* The current position is : \n");
		//printf("*M* x: %f , y: %f, dir: %f \n", my_pos.x, my_pos.y, my_pos.dir);
		//printf("difference time : %ld", clock() - t0);
		if (pthread_mutex_trylock(&detect_lock) == 0) {
			set_tacho_command_inx( motors[0], TACHO_STOP );
			set_tacho_command_inx( motors[1], TACHO_STOP );
			printf( "*M* ahah!\n" );
			printf("*Main* The current position is : \n");
			printf("*Main* x: %f , y: %f, dir: %f \n", my_pos.x, my_pos.y, my_pos.dir);
			printf("*Main* elle avait été détectée à : %f mm\n", value);
	
			if (value > 20) {
				va_chercher_maintenant(motors);
			} else {
				va_chercher_ac_creneau(motors);
			}
			grabtheball(motors);
			break;

		} 
        return EXIT_SUCCESS;
}
//thread de la détection
void* detection(void * args)
{
  	printf( "We are entering in the detection thread!\n" );
	uint8_t sn_sonar;
  	float value;
	float difference;
	pthread_mutex_lock(&detect_lock);
	for ( ; ; ){
		if (ev3_search_sensor(LEGO_EV3_US, &sn_sonar,0)){
		      //printf("*D* SONAR found, reading sonar...\n");
		      if ( !get_sensor_value0(sn_sonar, &value )) {
			value = 0;
		      }
		      printf( "*D* sonar :\r(%f) \n", value);
		      fflush( stdout );
		}
		printf("%f",value);
		//difference = 600 - value;
		//printf("%f",difference);
		if (value < 450) {
			d = value/19.5;
		      	printf( "*D* c'est tout pour moi! \n");
			pthread_mutex_unlock(&detect_lock);
			break;
		}
	}
        return EXIT_SUCCESS;
}


//le fameux main
int main( void )
{
  int i;
  uint8_t sn_touch;
  char s[ 256 ];
  int val;
  uint32_t n, ii;


  //struct timeb t0, t1;
  //struct thread_arguments thread_args;

#ifndef __ARM_ARCH_4T__
  /* Disable auto-detection of the brick (you have to set the correct address below) */
  ev3_brick_addr = "192.168.0.204";

#endif
  if ( ev3_init() == -1 ) return ( 1 );

#ifndef __ARM_ARCH_4T__
  printf( "The EV3 brick auto-detection is DISABLED,\nwaiting %s online with plugged tacho...\n", ev3_brick_addr );

#else
  printf( "Waiting tacho is plugged...\n" );

#endif
  while ( ev3_tacho_init() < 1 ) Sleep( 1000 );

  printf( "*** ( EV3 ) Hello! ***\n" );

  printf( "Found tacho motors:\n" );
  for ( i = 0; i < DESC_LIMIT; i++ ) {
    if ( ev3_tacho[ i ].type_inx != TACHO_TYPE__NONE_ ) {
      printf( "  type = %s\n", ev3_tacho_type( ev3_tacho[ i ].type_inx ));
      printf( "  port = %s\n", ev3_tacho_port_name( i, s ));
      printf("  port = %d %d\n", ev3_tacho_desc_port(i), ev3_tacho_desc_extport(i));
    }
  }

  //Run all sensors
  ev3_sensor_init();

  printf( "Found sensors:\n" );
  for ( i = 0; i < DESC_LIMIT; i++ ) {
    if ( ev3_sensor[ i ].type_inx != SENSOR_TYPE__NONE_ ) {
      printf( "  type = %s\n", ev3_sensor_type( ev3_sensor[ i ].type_inx ));
      printf( "  port = %s\n", ev3_sensor_port_name( i, s ));
      if ( get_sensor_mode( i, s, sizeof( s ))) {
        printf( "  mode = %s\n", s );
      }
      if ( get_sensor_num_values( i, &n )) {
        for ( ii = 0; ii < n; ii++ ) {
          if ( get_sensor_value( ii, i, &val )) {
            printf( "  value%d = %d\n", ii, val );
          }
        }
      }
    }
  }
  if ( ev3_search_sensor( LEGO_EV3_TOUCH, &sn_touch, 0 )) {
    printf( "TOUCH sensor is found, press BUTTON for EXIT...\n" );
  }

  motor_init( &motors[0], &motors[1], &motors[2], &motors[3]);

  my_pos.x = 0;
  my_pos.y = 0;
  my_pos.dir = 0;


  //on lance les threads
  int retour1 = pthread_create(&thread_movement, NULL, movements, NULL);
  if (retour1 != 0)
  {
    perror("erreur thread movement");
    exit(EXIT_FAILURE);
  }
  //pthread_mutex_init(&pos_lock, NULL);
  /*thread_args.motor0 = motors[0];
  thread_args.motor1 = motors[1];
  thread_args.motor2 = motors[2];
  thread_args.motor3 = motors[3];
  */
  int retour2 = pthread_create(&thread_detection, NULL, detection,  NULL);
  if (retour2 != 0)
  {
    perror("erreur thread detection");
    exit(EXIT_FAILURE);
  }

  //on termine les threads
  if (pthread_join(thread_movement, NULL)) 
  {
	  	  tacho_set_stop_action_hold(MOT_SX);
	  perror("pthread_join movement");
	  return EXIT_FAILURE;
  } 
  if (pthread_join(thread_detection, NULL)) 
  {
	  perror("pthread_join detection");
	  tacho_set_stop_action_hold(MOT_SX);
	  return EXIT_FAILURE;
  }

  ev3_uninit();
  
  donpapy(motors,d);
  printf("%f", d);
  grabtheball();
  printf( "*** ( EV3 ) Bye! ***\n" );

  return ( 0 );
}



//methode définissant reliant les moteurs physique à leur représentation dans le code
int motor_init(uint8_t *motor0, uint8_t* motor1, uint8_t* motor2, uint8_t* motor3) {
	int all_ok = 1;
	ev3_tacho_init();
	//initialisation moteur gauche
	if ( !ev3_search_tacho_plugged_in( MOT_SX, 0, motor0, 0 )) {
		fprintf( stderr, "Motor SX not found!\n" );
		set_tacho_stop_action_inx( *motor0, STOP_ACTION );
		set_tacho_command_inx( *motor0, TACHO_STOP );
		all_ok = 0;
	} else {
		set_tacho_stop_action_inx( *motor0, STOP_ACTION );
		set_tacho_command_inx( *motor0, TACHO_STOP );
	}
	//initialisation moteur droite
	if ( !ev3_search_tacho_plugged_in( MOT_DX, 0, motor1, 0 )) {
		fprintf( stderr, "Motor DX not found!\n" );
		set_tacho_stop_action_inx( *motor1, STOP_ACTION );
		set_tacho_command_inx( *motor1, TACHO_STOP );
		all_ok = 0;
	} else {
		set_tacho_stop_action_inx( *motor1, STOP_ACTION );
		set_tacho_command_inx( *motor1, TACHO_STOP );
	}
	//intialisation moteur pince
	if ( !ev3_search_tacho_plugged_in( MOT_PINCE, 0, motor2, 0 )) {
		fprintf( stderr, "Motor pince not found!\n" );
		set_tacho_stop_action_inx( *motor2, STOP_ACTION );
		set_tacho_command_inx( *motor2, TACHO_STOP );
		all_ok = 0;
	} else {
		set_tacho_stop_action_inx( *motor2, STOP_ACTION );
		set_tacho_command_inx( *motor2, TACHO_STOP );
	}
	//initialisation moteur catapulte
	if ( !ev3_search_tacho_plugged_in( MOT_CATAPULTE, 0, motor3, 0 )) {
		fprintf( stderr, "Motor catapulte not found!\n" );
		set_tacho_stop_action_inx( *motor3, STOP_ACTION );
		set_tacho_command_inx( *motor3, TACHO_STOP );
		all_ok = 0;
	} else {
		set_tacho_stop_action_inx( *motor3, STOP_ACTION );
		set_tacho_command_inx( *motor3, TACHO_STOP );
	}
	if (all_ok){
		stop_motor(*motor0);
		stop_motor(*motor1);
		stop_motor(*motor2);
		stop_motor(*motor3);
	}
	return all_ok;
}

