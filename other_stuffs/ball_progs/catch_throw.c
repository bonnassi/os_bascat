//Pauline

#include <stdio.h>
#include <stdlib.h>
#include "ev3.h"
#include "ev3_port.h"
#include "ev3_tacho.h"
#include "ev3_sensor.h"
// WIN32 /////////////////////////////////////////
#ifdef __WIN32__

#include <windows.h>

// UNIX //////////////////////////////////////////
#else

#include <unistd.h>
#define Sleep( msec ) usleep(( msec ) * 1000 )

//////////////////////////////////////////////////
#endif
const char const *color[] = { "?", "BLACK", "BLUE", "GREEN", "YELLOW", "RED", "WHITE", "BROWN" };
#define COLOR_COUNT  (( int )( sizeof( color ) / sizeof( color[ 0 ])))


int main( void )
{
  int i;
  uint8_t sn;
  FLAGS_T state;
  char s[ 256 ];

#ifndef __ARM_ARCH_4T__
  /* Disable auto-detection of the brick (you have to set the correct address below) */
  ev3_brick_addr = "192.168.0.204";

#endif
  if ( ev3_init() == -1 ) return ( 1 );

#ifndef __ARM_ARCH_4T__
  printf( "The EV3 brick auto-detection is DISABLED,\nwaiting %s online with plugged tacho...\n", ev3_brick_addr );

#else
  printf( "Waiting tacho is plugged...\n" );

#endif
  while ( ev3_tacho_init() < 1 ) Sleep( 1000 );

  printf( "*** ( EV3 ) Hello! ***\n" );

  printf( "Found tacho motors:\n" );
  for ( i = 0; i < DESC_LIMIT; i++ ) {
    if ( ev3_tacho[ i ].type_inx != TACHO_TYPE__NONE_ ) {
      printf( "  type = %s\n", ev3_tacho_type( ev3_tacho[ i ].type_inx ));
      printf( "  port = %s\n", ev3_tacho_port_name( i, s ));
      printf("  port = %d %d\n", ev3_tacho_desc_port(i), ev3_tacho_desc_extport(i));
    }
  }
  //Run motors in order from port A to D
  int port=65;
  //for (port=65; port<69; port++){  PARTIE PINCE
  if ( ev3_search_tacho_plugged_in(port,0, &sn, 0 )) {
      int max_speed;

      printf( "LEGO_EV3_M_MOTOR PINCE is found, run for 5 sec...\n" );
      get_tacho_max_speed( sn, &max_speed );
      printf("  max speed = %d\n", max_speed );
      set_tacho_stop_action_inx( sn, TACHO_COAST );

      set_tacho_speed_sp( sn, max_speed*0.1 );
      set_tacho_time_sp( sn, 900 );
      //set_tacho_ramp_up_sp( sn, 400 );
      //set_tacho_ramp_down_sp( sn, 300 );
      set_tacho_command_inx( sn, TACHO_RUN_TIMED );
      do {
        get_tacho_state_flags( sn, &state );
      } while ( state );

      printf( "on monte la balle\n" );
      get_tacho_state_flags( sn, &state );
      printf("Here is the robot position : %d\n", state);
      set_tacho_speed_sp( sn, max_speed*0.3 );
      set_tacho_time_sp( sn, 600 );
      //set_tacho_ramp_up_sp( sn, 50 );
      //set_tacho_ramp_down_sp( sn, 50 );
      //set_tacho_position_sp( sn, 90 );
      set_tacho_command_inx( sn, TACHO_RUN_TIMED );
	Sleep(2000);
	printf("We got down 2");
      do {
        get_tacho_state_flags( sn, &state );
      } while ( state );

      /* Wait tacho stop */
      Sleep( 1000 );
      printf( "on essaye dans l'autre sens\n" );
      get_tacho_state_flags( sn, &state );
      printf("Here is the robot position : %d\n", state);
      set_tacho_speed_sp( sn, - max_speed*0.3 );
      set_tacho_time_sp( sn, 800 );
      //set_tacho_ramp_up_sp( sn, 50 );
      //set_tacho_ramp_down_sp( sn, 50 );
      //set_tacho_position_sp( sn, 90 );
      set_tacho_command_inx( sn, TACHO_RUN_TIMED );
	Sleep(2000);
	printf("We got down 2");
      do {
        get_tacho_state_flags( sn, &state );
      } while ( state );
      Sleep( 1000 );
	printf("We got down");

    } else {
      printf( "LEGO_EV3_M_MOTOR 1 is NOT found\n" );
    }

  Sleep( 1000 );
  port=66;
  //PARTIE CATAPULTE
  if ( ev3_search_tacho_plugged_in(port,0, &sn, 0 )) {
      int max_speed;

      printf( "LEGO_EV3_M_MOTOR 1 is found, run for 5 sec...\n" );
      get_tacho_max_speed( sn, &max_speed );
      printf("  max speed = %d\n", max_speed );
      set_tacho_stop_action_inx( sn, TACHO_COAST );
      set_tacho_speed_sp( sn, max_speed);
      set_tacho_time_sp( sn, 400 );
      set_tacho_ramp_down_sp( sn, 500 );
      set_tacho_command_inx( sn, TACHO_RUN_TIMED );
      do {
        get_tacho_state_flags( sn, &state );
      } while ( state );
      /* Wait tacho stop */
      Sleep( 100 );
      printf( "on essaye dans l'autre sens\n" );
      set_tacho_speed_sp( sn, - max_speed*0.1 );
      set_tacho_time_sp( sn, 1100 );
      set_tacho_ramp_up_sp( sn, 10 );
      set_tacho_ramp_down_sp( sn, 300 );
      set_tacho_command_inx( sn, TACHO_RUN_TIMED );
      do {
        get_tacho_state_flags( sn, &state );
      } while ( state );

    } else {
      printf( "LEGO_EV3_M_MOTOR 1 is NOT found\n" );
    }

  
  ev3_uninit();
  printf( "*** ( EV3 ) Bye! ***\n" );

  return ( 0 );
}


