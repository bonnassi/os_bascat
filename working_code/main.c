//Pauline for the structure, Julie for the scenario


#include "functions.h"

int main(void)
{
        /* allocate a socket */
        oursock = socket(AF_BLUETOOTH, SOCK_STREAM, BTPROTO_RFCOMM);

        if (try_to_connect(oursock)==0) {
                //Creating the main thread stopping all the others when getting a stop message
                int main_thread_output = pthread_create(&thread_waiting_stop, NULL, waiting_stop, NULL);
                if (main_thread_output!=0){
                        printf("Error");
                        exit(EXIT_FAILURE);
                }
		//Initialisation of the motors and sensors
                ev3_sensor_init();
                motor_init( &motors[0], &motors[1], &motors[2], &motors[3]);
                throw_the_ball(motors, oursock);
                grab_the_ball(motors);
                throw_the_ball(motors, oursock);

                while (main_thread_output==0){


                        ///////////////          SCENARIO          ///////////////


			my_pos.x = 0; /*Initialising position*/
			my_pos.y = 0;
	  		my_pos.dir = 0;

			//Checking if there is a ball on OA
			IMAX = 50; dist_detect = 20;
	  		pthread_mutex_lock(&mov_stop_lock);
	  		int retour2 = pthread_create(&thread_detection, NULL, detection,  NULL);
	  		if (retour2 != 0){
	    			perror("erreur thread detection");
	    			exit(EXIT_FAILURE);
			}
	  		if (pthread_join(thread_detection, NULL)) {
		  		perror("pthread_join detection");
		  		tacho_set_stop_action_hold(MOT_SX);
		  		return EXIT_FAILURE;
	  		}

			//Checking if there is a ball on OC
	  		turn_left_motors(motors, MAX_SPEED/10, -180);
	  		printf("*Main* x: %f , y: %f, dir: %f \n", my_pos.x, my_pos.y, my_pos.dir);
			dist_detect = 54;
	  		retour2 = pthread_create(&thread_detection, NULL, detection,  NULL);
	  		if (retour2 != 0){
				perror("erreur thread detection");
	    			exit(EXIT_FAILURE);
			}
	  		if (pthread_join(thread_detection, NULL)) {
				perror("pthread_join detection");
		  		tacho_set_stop_action_hold(MOT_SX);
		  		return EXIT_FAILURE;
	  		}

			//Going against the wall
			turn_left_motors(motors, MAX_SPEED/10, -90);
	  		printf("*Main* x: %f , y: %f, dir: %f \n", my_pos.x, my_pos.y, my_pos.dir);
		  	printf("*Main* Navigating according to y axis... %d th iteration\n", i);
		  	navigatingYaxis = 1;
		  	while (navigatingYaxis == 1) {
				go_backwards_cm_foratime(motors, 55, MAX_SPEED/3);

			//Scanning the orange area, and then the green area
			  	IMAX = 100000; dist_detect = 0; time_tour = 4595822;
				pthread_mutex_unlock(&mov_stop_lock);
				int retour1 = pthread_create(&thread_movement, NULL, movements, NULL);
				if (retour1 != 0){
					perror("erreur thread movement");
			 		exit(EXIT_FAILURE);
				}
				retour2 = pthread_create(&thread_detection, NULL, detection,  NULL);
				if (retour2 != 0){
			 		perror("erreur thread detection");
			 		exit(EXIT_FAILURE);
			  	}
			  	if (pthread_join(thread_movement, NULL)){
					tacho_set_stop_action_hold(MOT_SX);
					perror("pthread_join movement");
					return EXIT_FAILURE;
			  	}
			  	if (pthread_join(thread_detection, NULL)){
					perror("pthread_join detection");
					tacho_set_stop_action_hold(MOT_SX);
					return EXIT_FAILURE;
			  	}
			  	printf("*Main* Done navigating according to y axis...\n");
			}

			//Scanning the white area and then the pink area
		  	turn_right_motors(motors, MAX_SPEED/10, 90);
			printf("*Main* Navigating according to x axis... %d th iteration\n", i);
			navigatingYaxis = 1;
			while (navigatingYaxis == 1){
				go_backwards_cm_foratime(motors, 55, MAX_SPEED/3);
			  	IMAX = 100000; time_tour = 6000000; dist_detect = 0;
				pthread_mutex_unlock(&mov_stop_lock);
				int retour1 = pthread_create(&thread_movement, NULL, movements, NULL);
				if (retour1 != 0){
			    		perror("erreur thread movement");
			    		exit(EXIT_FAILURE);
			  	}
			  	retour2 = pthread_create(&thread_detection, NULL, detection,  NULL);
			  	if (retour2 != 0){
			    		perror("erreur thread detection");
			    		exit(EXIT_FAILURE);
			  	}
			  	if (pthread_join(thread_movement, NULL)){
					tacho_set_stop_action_hold(MOT_SX);
					perror("pthread_join movement");
					return EXIT_FAILURE;
			  	}
			  	if (pthread_join(thread_detection, NULL)){
					perror("pthread_join detection");
					tacho_set_stop_action_hold(MOT_SX);
					return EXIT_FAILURE;
			  	}
			  	printf("*Main* Done navigating according to x axis...\n");
			}
                }
        }
	ev3_uninit();
       	printf( "*** ( EV3 ) Bye! ***\n" );
        close(oursock);
        return 0;
}
