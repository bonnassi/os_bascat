//Pauline

#include <stdarg.h>
#include <time.h>
#include <sys/socket.h>
#include <math.h>
#include <bluetooth/bluetooth.h>
#include <bluetooth/rfcomm.h>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include "ev3.h"
#include "ev3_port.h"
#include "ev3_tacho.h"
#include "ev3_sensor.h"
#include "ev3_dc.h"
#include "brick.h"

// WIN32 /////////////////////////////////////////
#ifdef _WIN32_
#include <windows.h>
// UNIX //////////////////////////////////////////
#else
#include <unistd.h>
#define Sleep( msec ) usleep(( msec ) * 1000 )
#endif

#define COLOR_COUNT  (( int )( sizeof( color ) / sizeof( color[ 0 ])))

//DEFINITION LINKED TO SERVER FUNCTIONS
#define SERV_ADDR	"5C:E0:C5:93:17:2C"     /* Server address*/
#define TEAM_ID		1                       /* Team ID */
#define MSG_ACK		0
#define MSG_START	1
#define MSG_STOP	2
#define MSG_KICK	3
#define MSG_SCORE	4
#define MSG_CUSTOM	8

//DEFINITIONS LINKED TO THE MOVING FUNCTIONS
#define MOT_DIR 1
#define MOT_PINCE 65
#define MOT_CATAPULTE 66
#define MOT_SX 67
#define MOT_DX 68
#define STOP_ACTION TACHO_BRAKE
#define MAX_SPEED 1050
typedef struct position {
	volatile float x;
	volatile float y;
	volatile float dir;
} position;
position my_pos;
#define WHEEL_RADIUS 0.0275
#define COMP_SX -1.0//0.9975 or 1.017
#define COMP_DX -1.0000
uint8_t motors[4];
pthread_mutex_t mov_stop_lock;
pthread_mutex_t mov_pause_lock;
pthread_mutex_t detect_lock;
float old_value_detect;
float new_value_detect;
time_t t0;
int IMAX;
int navigatingYaxis;
float dist_detect;
float time_tour;
#define TURN360 620


#define Sleep( msec ) usleep(( msec ) * 1000 )


//OTHER USEFULL DEFINITIONS
pthread_t thread_waiting_stop;
pthread_mutex_t endgame;
pthread_t thread_movement;
pthread_t thread_detection;
int oursock;
char s[256];
char string[58];
char type;
int i;
uint8_t sn;
FLAGS_T state;


//////////////////         DECLARATION OF THE FUNCTIONS         //////////////////

/*Initialisation*/
void stop_motor(uint8_t motor);
int motor_init(uint8_t *motor0, uint8_t *motor1, uint8_t *motor2, uint8_t *motor3);
/*Server*/
int try_to_connect(int oursock);
void* waiting_stop();
int read_from_server (int sock, char *buffer, size_t maxSize);
void send_message(int oursock, int score);
/*Ball manipulation*/
void grab_the_ball(uint8_t *motors);
void throw_the_ball(uint8_t *motors, int oursock);
/*Moving*/
void wait_motor_stop(uint8_t motor);
void go_forwards_cm_forever(uint8_t *motors, int speed);
void go_forwards_cm_foratime(uint8_t *motors, float cm, int speed);
void go_backwards_cm_foratime(uint8_t *motors, float cm, int speed);
void turn_right_motors(uint8_t *motors, int speed, int deg);
void turn_left_motors(uint8_t *motors, int speed, int deg);
void go_grab_the_ball(uint8_t *motors);
void go_grab_with_creneau(uint8_t *motors);
void go_back_home(uint8_t *motors);
void* movements();
void* detection();
