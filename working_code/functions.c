#include "functions.h"

const char const *color[] = { "?", "BLACK", "BLUE", "GREEN", "YELLOW", "RED", "WHITE", "BROWN" };


///////////////////      INITIALISATION OF MOTORS      ///////////////////


//Jordan
void stop_motor(uint8_t motor) {
	set_tacho_command_inx( motor, TACHO_STOP );
}

//Julie
int motor_init(uint8_t *motor0, uint8_t *motor1, uint8_t *motor2, uint8_t *motor3) 
{
	int all_ok = 1;
	ev3_tacho_init();
	//Left motor initialisation
	if ( !ev3_search_tacho_plugged_in( MOT_SX, 0, motor0, 0 )) {
		fprintf( stderr, "Motor Left not found!\n" );
		set_tacho_stop_action_inx( *motor0, STOP_ACTION );
		set_tacho_command_inx( *motor0, TACHO_STOP );
		all_ok = 0;
	} else {
		set_tacho_stop_action_inx( *motor0, STOP_ACTION );
		set_tacho_command_inx( *motor0, TACHO_STOP );
	}
	//Right motor initialisation
	if ( !ev3_search_tacho_plugged_in( MOT_DX, 0, motor1, 0 )) {
		fprintf( stderr, "Motor Right not found!\n" );
		set_tacho_stop_action_inx( *motor1, STOP_ACTION );
		set_tacho_command_inx( *motor1, TACHO_STOP );
		all_ok = 0;
	} else {
		set_tacho_stop_action_inx( *motor1, STOP_ACTION );
		set_tacho_command_inx( *motor1, TACHO_STOP );
	}
	//Clamb motor initialisation
	if ( !ev3_search_tacho_plugged_in( MOT_PINCE, 0, motor2, 0 )) {
		fprintf( stderr, "Clamb motor not found!\n" );
		set_tacho_stop_action_inx( *motor2, STOP_ACTION );
		set_tacho_command_inx( *motor2, TACHO_STOP );
		all_ok = 0;
	} else {
		set_tacho_stop_action_inx( *motor2, STOP_ACTION );
		set_tacho_command_inx( *motor2, TACHO_STOP );
	}
	//Launcher motor initialisation
	if ( !ev3_search_tacho_plugged_in( MOT_CATAPULTE, 0, motor3, 0 )) {
		fprintf( stderr, "Launcher motor not found!\n" );
		set_tacho_stop_action_inx( *motor3, STOP_ACTION );
		set_tacho_command_inx( *motor3, TACHO_STOP );
		all_ok = 0;
	} else {
		set_tacho_stop_action_inx( *motor3, STOP_ACTION );
		set_tacho_command_inx( *motor3, TACHO_STOP );
	}
	if (all_ok){
		stop_motor(*motor0);
		stop_motor(*motor1);
		stop_motor(*motor2);
		stop_motor(*motor3);
	}
	return all_ok;
}



///////////////////      FUNCTIONS RELATED TO THE CONNECTION TO THE SERVER      ///////////////////




/* Function connecting to the server through a given socket (created in the main function) */
//Pauline
int try_to_connect(int oursock) {
	struct sockaddr_rc addr = { 0 };
	int status;
	// set the connection parameters (who to connect to)
	addr.rc_family = AF_BLUETOOTH;
	addr.rc_channel = (uint8_t) 1;
	str2ba (SERV_ADDR, &addr.rc_bdaddr);
	// connect to server 
	status = connect(oursock, (struct sockaddr *)&addr, sizeof(addr));
	// if connected 
	if( status == 0 ) {
		char string[58];
		//Wait for START message
		read_from_server (oursock, string, 9);
		if (string[4] == MSG_START) {
			printf ("Received start message!\n");
			return 0;
		}
		sleep (5);
	} else {
		fprintf (stderr, "Failed to connect to server...\n");
		sleep (2);
		exit (EXIT_FAILURE);
	}

	exit(EXIT_FAILURE);
}

/*Function listening to messages of the server*/
//Pauline
int read_from_server (int sock, char *buffer, size_t maxSize) {
	int bytes_read = read (sock, buffer, maxSize);
	if (bytes_read <= 0) {
		fprintf (stderr, "Server unexpectedly closed connection...\n");
		close (oursock);
		exit (EXIT_FAILURE);
	}
	printf ("[DEBUG] received %d bytes\n", bytes_read);
	return bytes_read;
}

/*Function locking a mutex until the robot gets a stop/kicked message. */
//Pauline
void* waiting_stop() {

        printf("I'm waiting for the stop message");
        pthread_mutex_lock(&endgame);
        while(1){
                //Wait for stop message
                read_from_server (oursock, string, 58);
                type = string[4];
                if (type ==MSG_STOP){
                        pthread_mutex_unlock(&endgame);
                }
        }
}

/*Function sending a message to the server to tell how many points the robot thinks it should get after having sent a ball.*/
//Pauline
void send_message(int oursock, int score) {
        printf ("I'm going to send a message...\n");
        string[2] = TEAM_ID;
        string[3] = 0xFF;
        string[4] = MSG_SCORE;
        string[5] = score;          /* How many points we got */
        write(oursock, string, 6);
        Sleep(1000);
}



///////////////////      FUNCTIONS RELATED TO THE BALL MANIPULATION      ///////////////////


//Pauline
void grab_the_ball (uint8_t *motors){

	int max_speed;
	printf( "PINCE is found, run for 5 sec...\n" );
	get_tacho_max_speed( motors[2], &max_speed );
	printf("  max speed = %d\n", max_speed );
	set_tacho_stop_action_inx( motors[2], TACHO_COAST );
	//Grabing the ball slowly so it doesn't get pushed away
	set_tacho_speed_sp( motors[2], max_speed*0.1 );
	set_tacho_time_sp( motors[2], 900 );
	set_tacho_command_inx( motors[2], TACHO_RUN_TIMED );
	do {
		get_tacho_state_flags( motors[2], &state );
	} while ( state );
	//Taking the ball up to the launcher
	printf( "Taking the ball up\n" );
	set_tacho_speed_sp( motors[2], max_speed*0.3 );
	set_tacho_time_sp( motors[2], 700 );
	set_tacho_command_inx( motors[2], TACHO_RUN_TIMED );
	Sleep(1000);
	do {
		get_tacho_state_flags( motors[2], &state );
	} while ( state );
	//Giving the ball and taking the clamp down
	Sleep( 1000 );
	printf( "Giving the ball and taking the clamp down\n" );
	set_tacho_speed_sp( motors[2], - max_speed*0.3 );
	set_tacho_time_sp( motors[2], 900 );
	set_tacho_command_inx( motors[2], TACHO_RUN_TIMED );
	Sleep(1000);
	do {
        get_tacho_state_flags( motors[2], &state );
	} while ( state );
	Sleep( 1000 );
	printf("We got down");
}

//Pauline
void throw_the_ball(uint8_t *motors, int oursock) {
	uint8_t sn_color;
	int val;
	int doilaunch;
	//Checking 3 times if there is a ball in the launcher with the sensor color
	for (int i = 0 ; i < 3 ; i++){
		if ( ev3_search_sensor( LEGO_EV3_COLOR, &sn_color, 0 )) {
			printf( "COLOR sensor is found, reading COLOR...\n" );
			if ( !get_sensor_value( 0, sn_color, &val ) || ( val < 0 ) || ( val >= COLOR_COUNT )) {
				val = 0;
			}
      			printf( "\r(%s) \n", color[ val ]);
      			if (val == 0) {
        			printf("\r Well there is no ball \n");
        			doilaunch = 1;
        			break;
      			}
      			fflush( stdout );
      			Sleep(200);
    		}
    		doilaunch = 0;
	}
	//If there is a ball, launch it
	if ( doilaunch == 0) {
		int max_speed;
		printf( "LAUNCHER_MOTOR is found, run for 5 sec...\n" );
		get_tacho_max_speed( motors[3], &max_speed );
		printf("  max speed = %d\n", max_speed );
		set_tacho_stop_action_inx( motors[3], TACHO_COAST );
		set_tacho_speed_sp( motors[3], max_speed);
		set_tacho_time_sp( motors[3], 400 );
		set_tacho_ramp_down_sp( motors[3], 500 );
		set_tacho_command_inx( motors[3], TACHO_RUN_TIMED );
		do {
 			get_tacho_state_flags( motors[3], &state );
		} while ( state );
		Sleep( 100 );
		printf( "Coming back\n" );
		set_tacho_speed_sp( motors[3], - max_speed*0.1 );
		set_tacho_time_sp( motors[3], 1100 );
		set_tacho_ramp_up_sp( motors[3], 10 );
		set_tacho_ramp_down_sp( motors[3], 300 );
		set_tacho_command_inx( motors[3], TACHO_RUN_TIMED );
		do {
 			get_tacho_state_flags( motors[3], &state );
      		} while ( state );
		//Informing the server that the robot thinks it has put a ball in the basket
		send_message(oursock, 3); /*The score is always 3, because the robot always launches from the starting area*/
	} else {
      		if (doilaunch == 1) {
        		printf("\rThere were no ball, so I did not launch anything\n");
      		} else {
        		printf("\r LAUNCHER_MOTOR FOR LAUNCHING is NOT found\n" );
		}
    	}
}




///////////////////      FUNCTIONS RELATED TO THE MOVEMENTS      ///////////////////

/*Commonly used function to stop the robot's activity*/
//Jordan
void wait_motor_stop(uint8_t motor) {
 	FLAGS_T state;
 	do {
 	    get_tacho_state_flags( motor, &state );
 	} while ( state );
}

/*Function making the robot go ahead without knowing when it will be interrupted (called in a movement thread with mutex)*/
//Abbe
void go_forwards_cm_forever(uint8_t *motors, int speed) {
	multi_set_tacho_stop_action_inx( motors, STOP_ACTION );
	set_tacho_speed_sp( motors[0], speed * COMP_SX);
	set_tacho_speed_sp( motors[1], speed * COMP_DX);
	set_tacho_command_inx( motors[0], TACHO_RUN_FOREVER);
	set_tacho_command_inx( motors[1], TACHO_RUN_FOREVER );
}

/*Function making the robot go ahead until it reaches the defined "cm" distance*/
//Julie
void go_forwards_cm_foratime(uint8_t *motors, float cm, int speed) {
	float time = ((360.0*cm)/(2*M_PI*WHEEL_RADIUS)/speed)*10;
	multi_set_tacho_stop_action_inx( motors, STOP_ACTION );
	set_tacho_speed_sp( motors[0], speed * COMP_SX);
	set_tacho_speed_sp( motors[1], speed * COMP_DX);
	set_tacho_time_sp( motors[0], time );
	set_tacho_time_sp( motors[1], time );
	set_tacho_command_inx( motors[0], TACHO_RUN_TIMED);
	set_tacho_command_inx( motors[1], TACHO_RUN_TIMED );
	wait_motor_stop(motors[0]);
	wait_motor_stop(motors[1]);
	my_pos.x = my_pos.x + cm * sin((my_pos.dir * M_PI) / 180.0);
	my_pos.y = my_pos.y + cm * cos((my_pos.dir * M_PI) / 180.0);
}

/*Function making the robot go ahead until it reaches the defined "cm" distance*/
//Julie
void go_backwards_cm_foratime(uint8_t *motors, float cm, int speed) {
	float time = ((360.0*cm)/(2*M_PI*WHEEL_RADIUS)/speed)*10;
	speed = -speed;
	printf("time %f \n", time);
	multi_set_tacho_stop_action_inx( motors, STOP_ACTION );
	set_tacho_speed_sp( motors[0], speed * COMP_SX);
	set_tacho_speed_sp( motors[1], speed * COMP_DX);
	set_tacho_time_sp( motors[0], time );
	set_tacho_time_sp( motors[1], time );
	set_tacho_command_inx( motors[0], TACHO_RUN_TIMED);
	set_tacho_command_inx( motors[1], TACHO_RUN_TIMED );
	wait_motor_stop(motors[0]);
	wait_motor_stop(motors[1]);
	my_pos.x = my_pos.x - cm * sin((my_pos.dir * M_PI) / 180.0);
	my_pos.y = my_pos.y - cm * cos((my_pos.dir * M_PI) / 180.0);
}

/*Function making the robot turn to the right*/
//Julie
void turn_right_motors(uint8_t *motors, int speed, int deg) {
        multi_set_tacho_stop_action_inx( motors, STOP_ACTION );
        multi_set_tacho_speed_sp( motors, speed );
        multi_set_tacho_ramp_up_sp( motors, 0 );
        multi_set_tacho_ramp_down_sp( motors, 0 );
        set_tacho_position_sp( motors[0], MOT_DIR*(TURN360*deg)/360 );
        set_tacho_position_sp( motors[1], -MOT_DIR*(TURN360*deg)/360 );
        multi_set_tacho_command_inx( motors, TACHO_RUN_TO_REL_POS );
	wait_motor_stop(motors[0]); wait_motor_stop(motors[1]);
	my_pos.dir = ((((int)my_pos.dir + deg) % 360 ) + 360 ) % 360;
	if (my_pos.dir > 180) {
		my_pos.dir -= 360;
	}
}

/*Function making the robot turn to the left*/
//Julie
void turn_left_motors(uint8_t *motors, int speed, int deg) {
        multi_set_tacho_stop_action_inx( motors, STOP_ACTION );
        multi_set_tacho_speed_sp( motors, speed );
        multi_set_tacho_ramp_up_sp( motors, 0 );
        multi_set_tacho_ramp_down_sp( motors, 0 );
        set_tacho_position_sp( motors[0], -MOT_DIR*(TURN360*deg)/360 );
        set_tacho_position_sp( motors[1], MOT_DIR*(TURN360*deg)/360 );
        multi_set_tacho_command_inx( motors, TACHO_RUN_TO_REL_POS );
	wait_motor_stop(motors[0]); wait_motor_stop(motors[1]);
	my_pos.dir = ((((int)my_pos.dir + deg) % 360 ) + 360 ) % 360;
	if (my_pos.dir > 180) {
		my_pos.dir -= 360;
	}
}

/*Function making the robot go to the ball and keep it between the clamp*/
//Julie & Abbe
void go_grab_the_ball(uint8_t *motors) {
	go_forwards_cm_foratime(motors, 17, MAX_SPEED/10);
	printf("*Main* x: %f , y: %f, dir: %f \n", my_pos.x, my_pos.y, my_pos.dir);
	Sleep(1000);
	turn_right_motors(motors, MAX_SPEED/6, -90);
	printf("*Main* The current position is : \n");
	printf("*Main* x: %f , y: %f, dir: %f \n", my_pos.x, my_pos.y, my_pos.dir);
	go_forwards_cm_foratime(motors, (new_value_detect - 12), MAX_SPEED/30);
	printf("*Main* The current position is : \n");
	printf("*Main* x: %f , y: %f, dir: %f \n", my_pos.x, my_pos.y, my_pos.dir);
}

/*Same function as above, taking into account that it cannot turn 90 degrees because the ball is too close*/
//Julie
void go_grab_with_creneau(uint8_t *motors) {
	go_backwards_cm_foratime(motors, 30-17, MAX_SPEED/10);
	printf("*Main* x: %f , y: %f, dir: %f \n", my_pos.x, my_pos.y, my_pos.dir);
	float deg = atanf(new_value_detect/30)* 180.0f / M_PI;
	Sleep(1000);
	turn_right_motors(motors, MAX_SPEED/6, -deg);
	printf("*Main* x: %f , y: %f, dir: %f \n", my_pos.x, my_pos.y, my_pos.dir);
	float d = hypotf(30, new_value_detect);
	go_forwards_cm_foratime(motors, d - 12, MAX_SPEED/30);
	printf("*Main* x: %f , y: %f, dir: %f \n", my_pos.x, my_pos.y, my_pos.dir);
}

/*Function making the robot go back to the start position so it can throw a ball*/
//Julie
void go_back_home(uint8_t *motors) {
	printf("GOBACKHOME \n");
	float dist = hypotf(my_pos.x, my_pos.y);
	float deg = my_pos.dir ;//+ 90 + atanf(my_pos.x/my_pos.y)* 180.0f / M_PI;
	float deg2;
	if (my_pos.x > 0) {
		if (my_pos.y > 0) {
			deg2 = deg + abs(atanf(my_pos.y/my_pos.x)* 180.0f / M_PI);
			deg2 = 360-deg2;
			printf("*in gbh* xpos ypos deg: %f + atnaf: %f = %f \n", deg, atanf(my_pos.y/my_pos.x)* 180.0f / M_PI, deg2);
			turn_right_motors(motors, MAX_SPEED/10, deg2);
			printf("*in gbh* x: %f , y: %f, dir: %f \n", my_pos.x, my_pos.y, my_pos.dir);
			go_forwards_cm_foratime(motors, dist, MAX_SPEED/7);
			printf("*in gbh* x: %f , y: %f, dir: %f \n", my_pos.x, my_pos.y, my_pos.dir);
			turn_right_motors(motors, MAX_SPEED/10, - my_pos.dir);
			}
		else if (my_pos.y < 0) {
			deg2 = - deg + abs(atanf(my_pos.y/my_pos.x)* 180.0f / M_PI);
			printf("*in gbh* xpos yneg - deg: %f + atnaf: %f = %f \n", deg, atanf(my_pos.y/my_pos.x)* 180.0f / M_PI, deg2);
			turn_right_motors(motors, MAX_SPEED/10, deg2);
			printf("*in gbh* x: %f , y: %f, dir: %f \n", my_pos.x, my_pos.y, my_pos.dir);
			go_forwards_cm_foratime(motors, dist, MAX_SPEED/7);
			printf("*in gbh* x: %f , y: %f, dir: %f \n", my_pos.x, my_pos.y, my_pos.dir);
			turn_right_motors(motors, MAX_SPEED/10, -my_pos.dir);
			}
		else {
			if (my_pos.dir >0 ) {
				turn_right_motors(motors, MAX_SPEED/10, -my_pos.dir);
				go_forwards_cm_foratime(motors, dist, MAX_SPEED/7);
				}
			else {
				turn_right_motors(motors, MAX_SPEED/10, my_pos.dir);
				go_forwards_cm_foratime(motors, dist, MAX_SPEED/7);
				}
			}
	} else if (my_pos.x < 0){
		if (my_pos.y > 0) {
			deg2 = 180 + abs(atanf(my_pos.y/my_pos.x)* 180.0f / M_PI) - deg;
			printf("*in gbh* xneg ypos 18 + atanf: %f - deg : %f = %f \n", (atanf(my_pos.y/my_pos.x)* 180.0f / M_PI), deg, deg2);
			turn_right_motors(motors, MAX_SPEED/10, deg2);
			printf("*in gbh* x: %f , y: %f, dir: %f \n", my_pos.x, my_pos.y, my_pos.dir);
			go_forwards_cm_foratime(motors, dist, MAX_SPEED/7);
			printf("*in gbh* x: %f , y: %f, dir: %f \n", my_pos.x, my_pos.y, my_pos.dir);
			turn_right_motors(motors, MAX_SPEED/10, my_pos.dir);
			}
		else if (my_pos.y < 0) {
			deg2 = - deg + 180 - abs(atanf(my_pos.y/my_pos.x)* 180.0f / M_PI);
			printf("*in gbh* xneg yneg - deg: %f + 180 - atnaf: %f = %f \n", deg, (atanf(my_pos.y/my_pos.x)* 180.0f / M_PI), deg2);
			turn_right_motors(motors, MAX_SPEED/10, deg2);
			printf("*in gbh* x: %f , y: %f, dir: %f \n", my_pos.x, my_pos.y, my_pos.dir);
			go_forwards_cm_foratime(motors, dist, MAX_SPEED/7);
			printf("*in gbh* x: %f , y: %f, dir: %f \n", my_pos.x, my_pos.y, my_pos.dir);
			turn_right_motors(motors, MAX_SPEED/10, - my_pos.dir);
			}
		else {
			if (my_pos.dir >0 ) {
				turn_right_motors(motors, MAX_SPEED/10, 180-my_pos.dir);
				go_forwards_cm_foratime(motors, dist, MAX_SPEED/7);
				turn_right_motors(motors, MAX_SPEED/10, -180);
				}
			else {
				turn_right_motors(motors, MAX_SPEED/10, -(180+my_pos.dir));
				go_forwards_cm_foratime(motors, dist, MAX_SPEED/7);
				turn_right_motors(motors, MAX_SPEED/10, -180);
				}
			}
	} else {
		if (my_pos.y > 0) {
			if (my_pos.dir == 90 ) {
				turn_right_motors(motors, MAX_SPEED/10, 180);
				go_forwards_cm_foratime(motors, dist, MAX_SPEED/7);
				turn_right_motors(motors, MAX_SPEED/10, 90);
				}
			else if (my_pos.dir == -90 ) {
				go_forwards_cm_foratime(motors, dist, MAX_SPEED/7);
				turn_right_motors(motors, MAX_SPEED/10, 90);
				}
			}
		else if (my_pos.y < 0) {
			if (my_pos.dir == 90 ) {
				go_forwards_cm_foratime(motors, dist, MAX_SPEED/7);
				turn_right_motors(motors, MAX_SPEED/10, -90);
				}
			else if (my_pos.dir == -90 ) {
				turn_right_motors(motors, MAX_SPEED/10, 180);
				go_forwards_cm_foratime(motors, dist, MAX_SPEED/7);
				turn_right_motors(motors, MAX_SPEED/10, -90);
				}
			}
	}
}

/*Function associated to the position thread*/
//Julie
void* movements()
{
  	printf( "We are entering in the movements thread!\n" );
  	printf( "***\n" );
	printf("*M* The first position was : \n");
	printf("*M* x: %f , y: %f, dir: %f \n", my_pos.x, my_pos.y, my_pos.dir);
	t0 = clock();
	int aller = 0;
	pthread_mutex_lock(&mov_stop_lock);
	printf("*M* j'ai locké\n");
	for (int i=0 ;i<10000 ;i++ ){
		go_forwards_cm_forever(motors, MAX_SPEED/10);
		if (pthread_mutex_trylock(&detect_lock) == 0) {
			printf("*Main* x: %f , y: %f, dir: %f \n", my_pos.x, my_pos.y, my_pos.dir);
			float cm = 9.1594*pow(10, -6)*(clock() - t0) - 4.0021889;
			my_pos.x = my_pos.x + cm * sin((my_pos.dir * M_PI) / 180.0);
			my_pos.y = my_pos.y + cm * cos((my_pos.dir * M_PI) / 180.0);
			set_tacho_command_inx( motors[0], TACHO_STOP );
			set_tacho_command_inx( motors[1], TACHO_STOP );
			printf( "*M* THREAD Detection m'a dit de m'arreter!\n" );
			break;
		} else if (pthread_mutex_trylock(&endgame) == 0) {
			printf("*Main* x: %f , y: %f, dir: %f \n", my_pos.x, my_pos.y, my_pos.dir);
			float cm = 9.1594*pow(10, -6)*(clock() - t0) - 4.0021889;
			my_pos.x = my_pos.x + cm * sin((my_pos.dir * M_PI) / 180.0);
			my_pos.y = my_pos.y + cm * cos((my_pos.dir * M_PI) / 180.0);
			set_tacho_command_inx( motors[0], TACHO_STOP );
			set_tacho_command_inx( motors[1], TACHO_STOP );
			printf( "*M* THREAD Communication m'a dit de m'arreter!\n" );
			break; 
		} else if (clock() - t0 > time_tour && aller == 0) {
			pthread_mutex_lock(&mov_pause_lock);
			float cm = 9.1594*pow(10, -6)*(clock() - t0) - 4.0021889;
			my_pos.x = my_pos.x + cm * sin((my_pos.dir * M_PI) / 180.0);
			my_pos.y = my_pos.y + cm * cos((my_pos.dir * M_PI) / 180.0);
			set_tacho_command_inx( motors[0], TACHO_STOP );
			set_tacho_command_inx( motors[1], TACHO_STOP );
			printf("*Main* x: %f , y: %f, dir: %f \n", my_pos.x, my_pos.y, my_pos.dir);
			printf( "*M* didn't find anything on this side!\n" );
			turn_right_motors(motors, MAX_SPEED/6, 180);
			printf("*Main* x: %f , y: %f, dir: %f \n", my_pos.x, my_pos.y, my_pos.dir);
	  		go_backwards_cm_foratime(motors, 5, MAX_SPEED/3);
			pthread_mutex_unlock(&mov_pause_lock);
			printf("*M* j'ai délocké mov_pause_lock\n");
			aller +=1;
			t0 = clock();
		} else if (clock() - t0 > time_tour - 300000 && aller == 1) {
			float cm = 9.1594*pow(10, -6)*(clock() - t0) - 4.0021889;
			my_pos.x = my_pos.x + cm * sin((my_pos.dir * M_PI) / 180.0);
			my_pos.y = my_pos.y + cm * cos((my_pos.dir * M_PI) / 180.0);
			set_tacho_command_inx( motors[0], TACHO_STOP );
			set_tacho_command_inx( motors[1], TACHO_STOP );
			printf("*Main* x: %f , y: %f, dir: %f \n", my_pos.x, my_pos.y, my_pos.dir);
			printf( "*M* didn't find anything on this side either!\n" );
			go_backwards_cm_foratime(motors, 9.1594*pow(10, -6)*(time_tour/2) - 4.0021889, MAX_SPEED/7); 
			navigatingYaxis = 0;
			pthread_mutex_unlock(&mov_stop_lock);
			printf("*M* j'ai délocké mov_stop_lock\n");
			break;
		}
	}
        return EXIT_SUCCESS;
}


/*Function associated to the detection thread*/
//Julie & Abbe
void* detection()
{
  	printf( "We are entering in the detection thread!\n" );
	uint8_t sn_sonar;
	pthread_mutex_lock(&detect_lock);
	Sleep(1000);
	if (ev3_search_sensor(LEGO_EV3_US, &sn_sonar,0)){
		if (dist_detect == 0) {
			if ( !get_sensor_value0(sn_sonar, &old_value_detect )) {
				old_value_detect = 0;
			}
			old_value_detect = old_value_detect/10; //parce qu'on veut la valeur en cm et non en mm
			old_value_detect = 30;
			printf( "*D* sonar :\r(%f) \n", old_value_detect);
			fflush( stdout ); }
		else {old_value_detect = dist_detect;}
	}
	for (int i=0; i<IMAX ; i++){
		if (ev3_search_sensor(LEGO_EV3_US, &sn_sonar,0)){
		      if ( !get_sensor_value0(sn_sonar, &new_value_detect )) {
			new_value_detect = 0;
		      }
		      new_value_detect = new_value_detect/10;
		      if (pthread_mutex_trylock(&mov_pause_lock) == 0) {
				pthread_mutex_unlock(&mov_pause_lock);
		      }
		      fflush( stdout );
		}
		if (new_value_detect < old_value_detect && pthread_mutex_trylock(&mov_pause_lock) == 0) {
		      	printf( "*D* c'est tout pour moi! \n");
			pthread_mutex_unlock(&detect_lock);
			printf("*Detec* The current position is : \n");
			printf("*Detec* x: %f , y: %f, dir: %f \n", my_pos.x, my_pos.y, my_pos.dir);
			printf("*Detec* elle avait été détectée à : %f mm\n", new_value_detect);
			if (new_value_detect > 20) {
				go_grab_the_ball(motors);
			} else {
				go_grab_with_creneau(motors);
			}
			grab_the_ball(motors);
			go_back_home(motors);
			break;
		}
		else if (pthread_mutex_trylock(&mov_stop_lock) == 0) {
			printf( "*D* THREAD Movement m'a dit de m'arreter!\n" );
			break;
		}
		else if (pthread_mutex_trylock(&endgame) == 0) {
			printf( "*D* THREAD Communication m'a dit de m'arreter!\n" );
			break;
		}
		old_value_detect = new_value_detect;
	}
	pthread_mutex_unlock(&detect_lock);
        return EXIT_SUCCESS;
}

