This repository contains the .c programs we used to validate the midterm tests defined on the website :

* Test #1. Be able to drop a ball in the basket.

* Test #2. Be able to send a ball in the basket from the front area.

* Test #3. Be able to send in a reliable way a ball into the basket from the distant area.

* Test #4. Be able to find a ball and pick it up. The test is performed with a ball placed at a selected position at least at 15cm from the robot.

* Test #5. Be able to find a randomly placed ball and stay in the correct side.

* Test #6. Be able to connect to the server and inform the server when a ball is scored


The code for test #6 is not very clear : it actually just connects to the server and throws a ball from the back area (and stops every activity if the server kicks the team)
To test this file, run the server program (./server team), choose the team "Wehavenoname", and finally run test_6.