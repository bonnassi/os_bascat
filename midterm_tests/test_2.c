//Pauline

#include <stdio.h>
#include <stdlib.h>
#include "ev3.h"
#include "ev3_port.h"
#include "ev3_tacho.h"
#include "ev3_sensor.h"
// WIN32 /////////////////////////////////////////
#ifdef __WIN32__

#include <windows.h>

// UNIX //////////////////////////////////////////
#else

#include <unistd.h>
#define Sleep( msec ) usleep(( msec ) * 1000 )

//////////////////////////////////////////////////
#endif

int main( void )
{
  int i;
  uint8_t sn;
  FLAGS_T state;
  char s[ 256 ];

#ifndef __ARM_ARCH_4T__
  /* Disable auto-detection of the brick (you have to set the correct address below) */
  ev3_brick_addr = "10.0.42.82";

#endif
  if ( ev3_init() == -1 ) return ( 1 );

#ifndef __ARM_ARCH_4T__
  printf( "The EV3 brick auto-detection is DISABLED,\nwaiting %s online with plugged tacho...\n", ev3_brick_addr );

#else
  printf( "Waiting tacho is plugged...\n" );

#endif
  while ( ev3_tacho_init() < 1 ) Sleep( 1000 );

  printf( "*** ( EV3 ) Hello! ***\n" );

  printf( "Found tacho motor:\n" );
  for ( i = 0; i < DESC_LIMIT; i++ ) {
    if ( ev3_tacho[ i ].type_inx != TACHO_TYPE__NONE_ ) {
      printf( "  type = %s\n", ev3_tacho_type( ev3_tacho[ i ].type_inx ));
      printf( "  port = %s\n", ev3_tacho_port_name( i, s ));
      printf("  port = %d %d\n", ev3_tacho_desc_port(i), ev3_tacho_desc_extport(i));
    }
  }

  int port=66;
  //PARTIE CATAPULTE
  if ( ev3_search_tacho_plugged_in(port,0, &sn, 0 )) {
      int max_speed;

      printf( "LEGO_EV3_M_MOTOR 1 is found, run for 5 sec...\n" );
      get_tacho_max_speed( sn, &max_speed );
      printf("  max speed = %d\n", max_speed );
      set_tacho_stop_action_inx( sn, TACHO_COAST );
      set_tacho_speed_sp( sn, max_speed*0.5);
      set_tacho_time_sp( sn, 400 );
      //set_tacho_ramp_up_sp( sn, 0 );
      set_tacho_ramp_down_sp( sn, 500 );
      set_tacho_command_inx( sn, TACHO_RUN_TIMED );
      do {
        get_tacho_state_flags( sn, &state );
      } while ( state );
      /* Wait tacho stop */
      Sleep( 100 );
      printf( "on essaye dans l'autre sens\n" );
      set_tacho_speed_sp( sn, - max_speed*0.1 );
      set_tacho_time_sp( sn, 1000 );
      set_tacho_ramp_up_sp( sn, 10 );
      set_tacho_ramp_down_sp( sn, 300 );
      //set_tacho_position_sp( sn, 90 );
      set_tacho_command_inx( sn, TACHO_RUN_TIMED );
      do {
        get_tacho_state_flags( sn, &state );
      } while ( state );
      /*for ( i = 0; i < 8; i++ ) {
        set_tacho_command_inx( sn, TACHO_RUN_TO_REL_POS );
        Sleep( 500 );
      }*/

    } else {
      printf( "LEGO_EV3_M_MOTOR FOR LAUNCHING is NOT found\n" );
    }

  
  ev3_uninit();
  printf( "*** ( EV3 ) Bye! ***\n" );

  return ( 0 );
}

